import utils from '../_utils.js';

const lookup = utils.getLookupBy('tag')

const getd = utils.makeLookupGet(lookup)

export function get(req, res, next) {
  getd(req, res, next)
}
