import tvshows from './tvshow/_tvshows.js'
import mapping from './tvshow/_TVShowUtils'

const KEYS = [ 'type', 'id', 'title', 'sortTitle', 'year', 'dateAdded', 'posterUrl']

function sortByKey(array, key) {
    return array.sort(function(a, b) {
        var x = a[key]; var y = b[key];
        return ((x < y) ? -1 : ((x > y) ? 1 : 0))
    });
}
const sortTvshows = sortByKey(tvshows.map(m => mapping(m, KEYS)), 'sortTitle')

sortTvshows.forEach(m => {delete m.sortTitle})

const contents = JSON.stringify(sortTvshows)

export function get(req, res) {
  res.writeHead(200, {
    'Content-Type': 'application/json'
  });

  res.end(contents);
}
