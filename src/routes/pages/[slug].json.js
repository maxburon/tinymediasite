import movies from '../movie/_movies.js'
import mapping from '../movie/_MovieUtils'

const LENGTH = 10
const KEYS = ['type', 'id', 'title', 'sortTitle', 'year', 'dateAdded', 'posterUrl']

function sortByKey(array, key) {
  return array.sort(function(a, b) {
    var x = a[key]; var y = b[key];
    return ((x < y) ? -1 : ((x > y) ? 1 : 0))
  })
}
const sortMovies = sortByKey(movies.map(m => mapping(m, KEYS)), 'sortTitle')

sortMovies.forEach(m => {delete m.sortTitle})

export function get(req, res) {
  const { slug } = req.params

  if (!isNaN(slug) && parseInt(slug) >= 0) {
    const index = parseInt(slug)
    const skip = index * LENGTH
    const limit = LENGTH
    const contents = JSON.stringify(sortMovies.slice(skip, skip + limit))

    res.writeHead(200, {
      'Content-Type': 'application/json'
    })
    res.end(contents)
  } else {
    res.writeHead(404, {
      'Content-Type': 'application/json'
    })

    res.end(JSON.stringify({
      message: `Not found`
    }))
  }
}
