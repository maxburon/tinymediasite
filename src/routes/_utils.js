import movies from './movie/_movies.js';
import mapping from './movie/_MovieUtils.js'

const KEYS = ['type', 'id', 'sortTitle', 'title', 'year', 'dateAdded', 'posterUrl']

function makeGroupBy(key, objAcc, objInit) {
  const keys = KEYS.slice()
  keys.push(key)
  const projMovies = movies.map(m => mapping(m, keys))
  return projMovies.reduce((res, m) => {
    const objs = (Array.isArray(m[key])) ? m[key] : [m[key]]
    delete m[key]
    for (let obj of objs) {
      objAcc(res, obj, m)
    }
    return res
  }, objInit)

}

function getLookupBy(key, objectKey) {
  const groups = getGroupBy(key, objectKey)
  const lookup = new Map();
  for(let groupName in groups) {
    lookup.set(groupName, JSON.stringify(groups[groupName]))
  }

  return lookup
}

function getGroupBy(key, objectKey) {
  const objAcc = (res, obj, movie) => {
    const groupName = (objectKey) ? obj[objectKey] : obj
    if (res[groupName]) {
      if (res[groupName].findIndex(m => m.id === movie.id) === -1) {
        res[groupName].push(movie)
      }
    } else {
      res[groupName] = [movie]
    }
  }
  
  const groupBy = makeGroupBy(key,objAcc, {})

  for(let groupName in groupBy) {
    groupBy[groupName].sort((i1, i2) => (i2.sortTitle > i1.sortTitle) ? -1 : 1)
  }
  
  return groupBy
}

function getGroups(key) {
  const objAcc = (res, obj) => {
    res.push(obj)
    return res
  }
  return makeGroupBy(key, objAcc, []).filter((v, i, a) => a.indexOf(v) === i)
}

const makeLookupGet = lookup => {
  return (req, res, next) => {
    // the `slug` parameter is available because
    // this file is called [slug].json.js
    const { slug } = req.params;

    if (lookup.has(slug)) {
      res.writeHead(200, {
        'Content-Type': 'application/json'
      });

      res.end(lookup.get(slug));
    } else {
      res.writeHead(404, {
        'Content-Type': 'application/json'
      });

      res.end(JSON.stringify({
        message: `Not found`
      }));
    }
  }
}

export default {
  getLookupBy,
  getGroupBy,
  getGroups,
  makeLookupGet
}
