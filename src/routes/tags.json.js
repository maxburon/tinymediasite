import utils from './_utils.js';

const moviesByTag = utils.getGroupBy('tag')
const tags = []
for (let tag in moviesByTag) {
  tags.push({
    count: moviesByTag[tag].length,
    name: tag
  })
}

const selectTags = tags.filter(tag => tag.count > 2)
const contents = JSON.stringify(selectTags.sort((t1, t2) => { return (t1.name < t2.name) ? -1 : 1}))

export function get(req, res) {
  res.writeHead(200, {
    'Content-Type': 'application/json'
  });

  res.end(contents);
}
