import fs from 'fs'

const movies = JSON.parse(fs.readFileSync('./data/movies.json', 'utf8'))

export default movies;
