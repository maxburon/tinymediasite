import path from 'path'
const SOURCE_URL = process.env.TMS_ROOT


const utils = {
  type: movie => 'movie',
  id: movie => movie.tmdbid,
  title: movie => movie.title,
  sortTitle: movie => (typeof movie.sorttitle === 'string') ? movie.sorttitle : movie.title,
  year: movie => movie.year,
  rating: movie => movie.ratings && movie.ratings.rating && movie.ratings.rating.value,
  runtime: movie => movie.runtime,
  director: movie => movie.director,
  plot: movie => typeof movie.plot === 'string' && movie.plot,
  dateAdded: movie => movie.dateadded,
  tag: movie => arrayOrValue(movie.tag),
  genre: movie => arrayOrValue(movie.genre),
  directory: movie => movie.directory,
  directoryUrl: movie => SOURCE_URL + utils.directory(movie),
  posterUrl: movie => utils.directoryUrl(movie) + '/poster.jpg',
  bannerUrl: movie => utils.directoryUrl(movie) + '/banner.jpg',
  fanartUrl: movie => utils.directoryUrl(movie) + '/fanart.jpg',
  fileUrl: movie => utils.directoryUrl(movie) + '/' + fileName(movie) + fileExt(movie),
  fileAudioLangs: movie => (movie.fileinfo && movie.fileinfo.streamdetails) ? getUniqKey(movie.fileinfo.streamdetails.audio, 'language').filter(l => typeof l === 'string') : [],
  fileSubLangs: movie => (movie.fileinfo && movie.fileinfo.streamdetails) ? getUniqKey(movie.fileinfo.streamdetails.subtitle, 'language').filter(l => typeof l === 'string') : [],
  fileSubUrl: movie => {
    const subs = new Object()
    for (const lang of utils.fileSubLangs(movie)) {
      subs[lang] = utils.directoryUrl(movie) + '/' + fileName(movie)  + ((lang) ? '.'+lang : '')+'.srt'
    }
    return subs
  },
  actors: movie => arrayOrValue(movie.actor).map(person(movie))
}

const fileName = movie => {
  const title = movie.title
    .replace(/ ?\?/g, '')
    .replace(/ ?: ?/g, ' - ')
  return title + ((movie.year) ? ' ('+movie.year+')' :'')
}

const fileExt = movie => {
  if (!movie.original_filename) return ''
  const regex = /(\.[^/.]+)$/g
  const found = movie.original_filename.match(regex)
  return found[0]
}

const getUniqKey = (t, key) => {
  return arrayOrValue(t).map(o => o[key]).filter((v, i, a) => a.indexOf(v) === i)
}

const arrayOrValue = t => {
  return t ? ((Array.isArray(t)) ? t.filter(e=>e) : [t]) : []
}

const stringToPath = s => {
  return encodeURI(s.replace(/ /g, '_'))
}

const person = movie => (p => {
  return {
    fanartUrl: utils.directoryUrl(movie) + '/.actors/' + stringToPath(p.name) + '.jpg',
    role: (typeof p.role === "string") ? p.role : "",
    name: p.name
  }
})

const mapping = function (obj, ks) {
  const keys = ks || Object.keys(utils)
  const movie = new Object()
  for(let key of keys) {
    movie[key] = utils[key](obj)
  }
  return movie
}

export default mapping
