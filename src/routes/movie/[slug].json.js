import movies from './_movies.js';
import mapping from './_MovieUtils.js'

const lookup = new Map();
let movieMap = new Map();
let moviePerActor = new Map();

// initialization of the movie object and movie's actor counts
movies.forEach(obj => {
  const movie = mapping(obj)
  movieMap[movie.id] = movie;

  for (let actor of movie.actors) {
    if (moviePerActor[actor.name]) {
      moviePerActor[actor.name]++
    } else {
      moviePerActor[actor.name] = 1
    }
  }
});

// update of the movie's actor counts
for(let id in movieMap) {
  for(let actor of movieMap[id].actors) {
    actor.movieCount = moviePerActor[actor.name]
  }
}

// initialization of the lookup object
for (let id in movieMap) {
  lookup.set(id, JSON.stringify(movieMap[id]))
}

movieMap = null
moviePerActor = null

export function get(req, res, next) {
  // the `slug` parameter is available because
  // this file is called [slug].json.js
  const { slug } = req.params;

  if (lookup.has(slug)) {
    res.writeHead(200, {
      'Content-Type': 'application/json'
    });
    res.end(lookup.get(slug));
  } else {
    res.writeHead(404, {
      'Content-Type': 'application/json'
    });

    res.end(JSON.stringify({
      message: `Not found`
    }));
  }
}
