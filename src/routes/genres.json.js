import utils from './_utils.js';

const contents = JSON.stringify(utils.getGroups('genre'))

export function get(req, res) {
  res.writeHead(200, {
    'Content-Type': 'application/json'
  });

  res.end(contents);
}
