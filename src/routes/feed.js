import xml from 'xml'
import movies from './movie/_movies.js'
import mapping from './movie/_MovieUtils'

const HOST = process.env.HOST
const LENGTH = 20
const KEYS = ['id', 'title', 'plot', 'dateAdded', 'posterUrl']

function sortByKey (array, key) {
  return array.sort(function (a, b) {
    var x = a[key]; var y = b[key]
    return ((x < y) ? -1 : ((x > y) ? 1 : 0))
  })
}
const sortMovies = sortByKey(movies.map(m => mapping(m, KEYS)), 'dateAdded')
      .reverse()
      .slice(0, LENGTH)

const xmlObject = {
  rss: [
    {
      _attr: {
        version: '2.0',
        'xmlns:atom': 'http://www.w3.org/2005/Atom'
      }
    },
    {
      channel: [
        {
          'atom:link': {
            _attr: {
              href: HOST + '/feed',
              rel: 'self',
              type: 'application/rss+xml'
            }
          }
        },
        { title: 'tiny media site' },
        { link: HOST },
        { description: '' },
        { language: 'en-us' },
        ...sortMovies.map((movie) => {
          return {
            item: [
              { title: movie.title },
              { pubDate: new Date(movie.dateAdded).toUTCString() },
              { link: HOST + '/movie/' + movie.id },
              { guid: HOST + '/movie/' + movie.id },
              { description: { _cdata: `<img src="${movie.posterUrl}"/><p>${movie.plot}</p>` } }
            ]
          }
        })
      ]
    }
  ]
}

const contents = '<?xml version="1.0" encoding="UTF-8"?>' + xml(xmlObject)

export function get (req, res) {
  res.writeHead(200, {
    'Content-Type': 'application/rss+xml'
  })

  res.end(contents)
}
