import fs from 'fs'

const tvshows = JSON.parse(fs.readFileSync('./data/tvshows.json', 'utf8'))

export default tvshows;
