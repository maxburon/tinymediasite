const SOURCE_URL = process.env.TMS_ROOT
const path = require('path')

const utils = {
  type: tvshow => 'tvshow',
  id: tvshow => tvshow.id,
  title: tvshow => tvshow.title,
  sortTitle: tvshow => tvshow.sorttitle || tvshow.title,
  year: tvshow => tvshow.year,
  rating: tvshow => tvshow.rating,
  runtime: tvshow => tvshow.runtime,
  director: tvshow => tvshow.director,
  plot: tvshow => tvshow.plot,
  dateAdded: tvshow => tvshow.dateadded,
  tag: tvshow => arrayOrValue(tvshow.tag),
  genre: tvshow => arrayOrValue(tvshow.genre),
  directory: tvshow => tvshow.directory + '/' + encodeURIComponent(tvshow.title + ((tvshow.year) ?' ('+tvshow.year+')' : '')),
  directoryUrl: tvshow => SOURCE_URL + utils.directory(tvshow),
  bannerUrl: tvshow => utils.directoryUrl(tvshow) + '/banner.jpg',
  posterUrl: tvshow => utils.directoryUrl(tvshow) + '/poster.jpg',
  fanartUrl: tvshow => utils.directoryUrl(tvshow) + '/fanart.jpg',
  actors: tvshow => arrayOrValue(tvshow.actor).map(person(tvshow)),
  seasons: tvshow => arrayOrValue(tvshow.seasons).map(season(tvshow))
}

const getUniqKey = (t, key) => {
  return arrayOrValue(t).map(o => o[key]).filter((v, i, a) => a.indexOf(v) === i)
}

const arrayOrValue = t => {
  return t ? ((Array.isArray(t)) ? t.filter(e=>e) : [t]) : []
}

const stringToPath = s => {
  return s.replace(/ /g, '_')
}

const person = tvshow => (p => {
  return {
    fanartUrl: utils.directoryUrl(tvshow) + '/.actors/' + stringToPath(p.name) + '.jpg',
    role: p.role,
    name: p.name
  }
})

const season = tvshow => (s => {
  const episodes = arrayOrValue(s.episodes).map(ep => mapping(ep, null, episodeUtils(tvshow)))
  episodes.sort((e1,e2) => e1.episode - e2.episode)
  return {
    number: s.episodes[0] && s.episodes[0].season,
    episodes: episodes
  }
})

const episodeUtils = tvshow => {
  const t = {
    id: ep => ep.id,
    title: ep => ep.title,
    sortTitle: ep => ep.sorttitle || ep.title,
    year: ep => ep.year,
    rating: ep => ep.rating,
    runtime: ep => ep.runtime,
    plot: ep => ep.plot,
    episode: ep => parseInt(ep.episode),
    directory: ep => utils.directory(tvshow) + '/Season ' + ep.season,
    directoryUrl: ep => SOURCE_URL + t.directory(ep),
    thumbUrl: ep => t.directoryUrl(ep) + '/' + fileName(tvshow, ep) + '-thumb.jpg',
    fileUrl: ep => t.directoryUrl(ep) + '/' + fileName(tvshow, ep) + fileExt(ep),
    fileAudioLangs: ep => (ep.fileinfo && ep.fileinfo.streamdetails) ? getUniqKey(ep.fileinfo.streamdetails.audio, 'language'): [],
    fileSubLangs: ep => (ep.fileinfo && ep.fileinfo.streamdetails) ? getUniqKey(ep.fileinfo.streamdetails.subtitle, 'language') : [],
    fileSubUrl: (ep) => {
      const subs = new Object()
      for (const lang of t.fileSubLangs(ep)) {
        subs[lang] = t.directoryUrl(ep) + '/' + fileName(tvshow, ep) + ((lang) ? '.'+lang : '')+'.srt'
      }
      return subs
    }
  }
  return t
}

const fileName = (tvshow, ep) => {
  const season = (parseInt(ep.season) < 10) ? '0' + ep.season: ep.season
  const episode = (parseInt(ep.episode) < 10) ? '0' + ep.episode: ep.episode
  const title = `${tvshow.title} - S${season}E${episode} - ${ep.title}`
        .replace(/ ?\?/g, '')
        .replace(/ ?: ?/g, ' ')
  return title
}

const fileExt = movie => {
  if (!movie.original_filename) return ''
  const regex = /(\.[^/.]+)$/g
  const found = movie.original_filename.match(regex)
  return found[0]
}


const mapping = function (obj, ks, ut=utils) {
  const keys = ks || Object.keys(ut)
  const tvshow = new Object()
  for(let key of keys) {
    tvshow[key] = ut[key](obj)
  }
  return tvshow
}

export default mapping
