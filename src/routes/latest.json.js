import movies from './movie/_movies.js'
import mapping from './movie/_MovieUtils'

const LENGTH = 20
const KEYS = ['type','id', 'title', 'year', 'dateAdded', 'posterUrl']

function sortByKey(array, key) {
    return array.sort(function(a, b) {
        var x = a[key]; var y = b[key];
        return ((x < y) ? -1 : ((x > y) ? 1 : 0))
    });
}
const sortMovies = sortByKey(movies.map(m => mapping(m, KEYS)), 'dateAdded')
      .reverse()
      .slice(0, LENGTH)

sortMovies.forEach(m => {delete m.dateAdded})

const contents = JSON.stringify(sortMovies)

export function get(req, res) {
  res.writeHead(200, {
    'Content-Type': 'application/json'
  });

  res.end(contents);
}
