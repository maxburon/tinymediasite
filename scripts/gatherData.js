const fs = require('fs')
const Path = require('path')
const glob = require('glob')
const convert = require('xml-js')

// loading of configuration
require('dotenv').config()
const MOVIES_DIR = process.env.MOVIES_DIR.split(',')
const SERIES_DIR = process.env.SERIES_DIR.split(',')
const SEASON_PATTERN = 'Season*/'

const convertMovie = path => {
  return globPromise(path + '*.nfo')
    .then(files => {
      if (!files.length) {
        throw new Error('directory with no .nfo file: ' + path)
      }
      if (files.length > 1)
        console.log('these files are ignored: ' + JSON.stringify(files.splice(1)))
      return files[0]
    })
    .then(convertFile)
    .then(obj => {
      const movie = obj.movie
      movie.directory = path
      return movie
    })
    .catch(e => {
      console.error(e)
      return null
    })
}

const convertSerie = path => {
  // first convert the NFO file of serie
  return globPromise(path + '*.nfo')
    .then(paths => Promise.all(paths.map(convertFile)))
    .then(nfos => {
      for (let nfo of nfos) {
        if (nfo.tvshow) {
          return nfo
        } else {
          console.warn('file ' + nfo.nfoSourcePath + ' is not tvshow file')
          return null
        }
      }
    })
    .then(serieNfo => {
      if (!serieNfo) throw new Error('missing nfo in ' + path)
      return globPromise(path + SEASON_PATTERN)
        .then(paths => Promise.all(paths.map(convertSeason)))
        .then(seasons => {
          serieNfo.tvshow.seasons = seasons
          serieNfo.tvshow.directory = SERIES_DIR
          return serieNfo.tvshow
        })
    })
    .catch(e => {
      console.error(e)
      return null
    })
}

const convertSeason = path => {
  return globPromise(path + '*.nfo')
    .then(paths => {
      const promises = paths.map(p => {
        return convertFile(p)
          .catch(e => {
            console.error(e)
            return null
          })
      })
      return Promise.all(promises)
    })
    .then(nfos => nfos.filter(f => f))
    .then(nfos => {
      const episodes = []
      const season = {
        episodes: episodes
      }

      for (let nfo of nfos) {
        if (nfo.episodedetails) {
          episodes.push(nfo.episodedetails)
        } else {
          console.warn('file ' + nfo.nfoSourcePath + ' is not episode file')
        }
      }
      return season
    })
}

const globPromise = pattern => {
  return new Promise((resolve, reject) => {
    glob(pattern, (err, files) => {
      if (err) return reject(err)
      resolve(files)
    })
  })
}

const convertFile = path => {
  return new Promise((resolve, reject) => {
    fs.readFile(path, 'utf8', (err, xml) => {
      if (err) return reject(err)
      const obj = convert.xml2js(xml, { compact: true, spaces: 4 })
      const smp = simplifyConversion(obj)
      smp.nfoSourcePath = path
      resolve(smp)
    })
  })
}

const simplifyConversion = (obj) => {
  let smp
  if (Array.isArray(obj)) {
    smp = []
    for (const val of obj) {
      smp.push(simplifyConversion(val))
    }
  } else if (typeof obj === 'object') {
    if (obj._text) {
      return obj._text
    }
    smp = new Object()
    for (const key in obj) {
      if (!key.startsWith('_')) {
        smp[key] = simplifyConversion(obj[key])
      }
    }
  }
  return smp
}

const handleDirectories = (paths, type) => {
  const relativeOutput = (type === 'movie') ? '../data/movies.json' : '../data/tvshows.json'
  const output = Path.join(__dirname, relativeOutput)
  const handler = (type === 'movie') ? convertMovie : convertSerie

  const promises = paths.map(path => {
    return globPromise(path + '/*/')
      .then(dirs => Promise.all(dirs.map(handler)))
      .then(medias => medias.filter(m => m))
  })

  // unnested medias
  return Promise.all(promises)
    .then(nested => nested.flat())
    .then(medias => fs.promises.writeFile(output, JSON.stringify(medias)))
}

// create data directory if needed
fs.mkdirSync(Path.join(__dirname, '../data'), { recursive: true })

handleDirectories(MOVIES_DIR, 'movie')

handleDirectories(SERIES_DIR, 'serie')
