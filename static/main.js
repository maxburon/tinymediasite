  // lazy loading of images

  let images = [...document.querySelectorAll('[loading=lazy]')]

  const interactSettings = {
    root: null,
    rootMargin: '0px 0px 200px 0px'
  }

  function onIntersection(imageEntites) {
    imageEntites.forEach(image => {
      if (image.isIntersecting) {
        observer.unobserve(image.target)
        if (image.target.dataset.src) {
          image.target.src = image.target.dataset.src
        } else {
          image.target.src = image.target.dataset.path + image.target.dataset.file.replace(/ /g,'_')
        }
      }
    })
  }

  let observer = new IntersectionObserver(onIntersection, interactSettings)

  images.forEach(image => observer.observe(image))
